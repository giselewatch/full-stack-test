# Teste full stack

# Backend

## Tecnologias Utilizadas

- Node.js
- Express.js
- Prisma
- Postgres

## Endpoints

- `/register` (POST): Registra um novo usuário.
- `/login` (POST): Faz login de um usuário.
- `/logout` (POST): Faz logout de um usuário.
- `/user/:id` (DELETE): Deleta um usuário (requer autenticação).
- `/user/:id` (GET): Obtém informações de um usuário (requer autenticação).
- `/lists` (GET): Obtém uma lista de filmes de uma API externa.
- `/lists/:id` (GET): Obtém detalhes de um filme específico de uma API externa.

# Front-end

## Tecnologias Utilizadas

- Next.js
- Sass

## Telas

### Troca de temas
![troca de temas](images/troca_tema.gif)

### Home desktop

![home](images/home-desktop.png)

### Home mobile

![home mobile](images/home-mobile.png)

### Tela de Login Mobile

![Tela de Login Mobile](images/login-mobile.png)

### Tela de Cadastro Desktop

![Tela de Cadastro Desktop](images/cadastro-desktop.png)

### Tela de Cadastro Mobile

![Tela de Cadastro Mobile](images/cadastro-mobile.png)

### Dados da conta

![Dados da conta](images/light_account.png)


