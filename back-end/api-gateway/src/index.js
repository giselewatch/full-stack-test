require("dotenv-safe").config();
const jwt = require("jsonwebtoken");
const http = require("http");
const express = require("express");
const httpProxy = require("express-http-proxy");
const app = express();
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");
var logger = require("morgan");
const helmet = require("helmet");
const cors = require('cors');
app.use(express.json());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(cors({
  origin: 'http://localhost:3000',
  credentials: true
}));

const usersServiceProxy = httpProxy("http://localhost:3002");
const listsServiceProxy = httpProxy("http://localhost:3001");
app.post('/login', (req, res, next) => {
  usersServiceProxy(req, res, (result) => {
    if (result instanceof Error) {
      next(result);
    } else {
      try {
        const response = JSON.parse(result.body);
        if (response.error) {
          res.status(400).json({ message: response.error });
        } else {
          const token = response.token;
          res.json({ auth: true, token: token });
        }
      } catch (err) {
        next(err);
      }
    }
  });
});

app.post('/register', (req, res, next) => {
  usersServiceProxy(req, res, next)
})

app.get('/users/:id', (req, res, next) => {
  usersServiceProxy(req, res, next)
})

app.delete('/users/:id', (req, res, next) => {
  usersServiceProxy(req, res, next)
})

app.get('/lists/:id', (req, res, next) => {
  listsServiceProxy(req, res, next)
})

app.get('/lists', (req, res, next) => {
  listsServiceProxy(req, res, next)
})

app.post('/logout', function(req, res) {
  res.json({auth: false, token: null});
})


app.use(logger("dev"));
app.use(helmet());

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

var server = http.createServer(app);
server.listen(3003);
