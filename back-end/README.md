# Backend

## Tecnologias Utilizadas

- Node.js
- Express.js
- Prisma
- Postgres

## Uso

1. Para iniciar os serviços, execute `npm run dev`.
2. Para iniciar o API Gateway, execute `node src/index.js`.

## Endpoints

- `/register` (POST): Registra um novo usuário.
- `/login` (POST): Faz login de um usuário.
- `/logout` (POST): Faz logout de um usuário.
- `/user/:id` (DELETE): Deleta um usuário (requer autenticação).
- `/user/:id` (GET): Obtém informações de um usuário (requer autenticação).
- `/lists` (GET): Obtém uma lista de filmes de uma API externa.
- `/lists/:id` (GET): Obtém detalhes de um filme específico de uma API externa.
