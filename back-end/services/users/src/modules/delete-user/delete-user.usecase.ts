import { prismaClient } from "../../infra/database/prismaClient";

type DeleteUserRequest = {
  id: string;
  token: string;
};

export class DeleteUserUseCase {
  constructor() {}

  async execute(data: DeleteUserRequest) {

    const user = await prismaClient.user.findFirst({
      where: {
        id: data.id,
      },
    });

    if (!user) {
      throw new Error("User not found");
    }

    await prismaClient.user.delete({
      where: {
        id: data.id,
      },
    });
  }
}
