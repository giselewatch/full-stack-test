import { Request, Response } from 'express';
import { DeleteUserUseCase } from './delete-user.usecase';

export class DeleteUserController {
  constructor(private deleteUserUseCase: DeleteUserUseCase) {}

  async handle(req: Request, res: Response): Promise<Response> {
    const { id } = req.params;
    const token = req.headers.authorization?.split(' ')[1];
    try {
      // @ts-ignore
      await this.deleteUserUseCase.execute({ id, token });
      return res.status(204).send();
    } catch (err) {
      const error = err as Error;
      return res.status(400).json({
        message: error.message || 'Unexpected error.',
      });
    }
  }
}
