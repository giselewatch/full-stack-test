import jwt from 'jsonwebtoken';
import { prismaClient } from "../../infra/database/prismaClient";

type LoginRequest = {
  email: string;
  password: string;
};

export class LoginUseCase {
  constructor() { }

  async execute(data: LoginRequest) {
    const user = await prismaClient.user.findFirst({
      where: {
        email: data.email,
      },
    });

    if (!user) {
      throw new Error("User not found");
    }

    if (!process.env.SECRET) {
      throw new Error("JWT secret not defined");
    }

    console.log(process.env.SECRET);

    const token = jwt.sign({ id: user.id }, process.env.SECRET, {
      expiresIn: 300
    });

    return { ...user, token: token };
  }
}
