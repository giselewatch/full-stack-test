import { Request, Response } from 'express';
import { LoginUseCase } from './login.usecase';

export class LoginController {
  constructor() { }

  async handle(request: Request, response: Response) {
    const useCase = new LoginUseCase();
    try {
      const result = await useCase.execute(request.body);
      return response.json(result);
    } catch (err) {
      return response.status(400).json({ message: (err as Error).message });
    }
  }
}
