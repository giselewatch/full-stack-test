import { prismaClient } from "../../infra/database/prismaClient";

type CreateClientRequest = {
  name: string;
  password: string;
  email: string;
  phone: string;
};

export class CreateClientUseCase {
  constructor() {}

  async execute(data: CreateClientRequest) {
    const user = await prismaClient.user.findFirst({
      where: {
        email: data.email,
      },
    });

    if (user) {
      throw new Error("User already exists");
    }

    const UserCreated = await prismaClient.user.create({
      data: {
        ...data,
      },
    });

    return UserCreated;
  }
}