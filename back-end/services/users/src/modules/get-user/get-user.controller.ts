import { Request, Response } from 'express';
import { GetUserUseCase } from "./get-user.usecase";

export class GetUserController {
  constructor() { }

  async handle(request: Request, response: Response) {
    const useCase = new GetUserUseCase();
    try {
      const result = await useCase.execute({ id: request.params.id });
      return response.json(result);
    } catch (err) {
      return response.status(400).json(err);
    }
  }
}
