import { prismaClient } from "../../infra/database/prismaClient";

type GetUserRequest = {
  id: string;
};

export class GetUserUseCase {
  constructor() {}

  async execute(data: GetUserRequest) {
    const user = await prismaClient.user.findFirst({
      where: {
        id: data.id,
      },
    });

    if (!user) {
      throw new Error("User not found");
    }

    return user;
  }
}