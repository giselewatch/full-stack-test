import { LoginController } from '../modules/login-user/login.controller';
import { CreateUserController } from '../modules/create-user/create-user.controller';
import { GetUserController } from '../modules/get-user/get-user.controller';
import { DeleteUserController } from '../modules/delete-user/delete-user.controller';
// import { EditUserController } from '../modules/edit-client/edit-client.controller';

import { Router } from 'express';
import { DeleteUserUseCase } from '../modules/delete-user/delete-user.usecase';

const router = Router();

const deleteUserUseCase = new DeleteUserUseCase();

router.post('/login', (request, response) => {
  new LoginController().handle(request, response);
});

router.post('/register', (request, response) => {
  new CreateUserController().handle(request, response);
});

router.get('/users/:id', (request, response) => {
  new GetUserController().handle(request, response);
});

router.delete('/users/:id', (request, response) => {
  new DeleteUserController(deleteUserUseCase).handle(request, response);
});

// router.put('/customers/:id', (request, response) => {
//   new EditUserController().handle(request, response);
// });

export { router };