"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.router = void 0;
var lists_controller_1 = require("../modules/lists.controller");
var express_1 = require("express");
var lists_usecase_1 = require("../modules/lists.usecase");
var router = (0, express_1.Router)();
exports.router = router;
router.get('/lists', function (request, response) {
    new lists_controller_1.ListsController(new lists_usecase_1.PopularMoviesUseCase()).handle(request, response);
});
router.get('/lists/:id', function (request, response) {
    new lists_controller_1.ListsController(new lists_usecase_1.FetchGenresMoviesUseCase()).handle(request, response);
});
