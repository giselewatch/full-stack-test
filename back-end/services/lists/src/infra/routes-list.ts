import { ListsController } from '../modules/lists.controller'

import { Router } from 'express';
import { PopularMoviesUseCase, FetchGenresMoviesUseCase } from '../modules/lists.usecase';

const router = Router();

router.get('/lists', (request, response) => {
  new ListsController(
    new PopularMoviesUseCase()
  ).handle(request, response);
});

router.get('/lists/:id', (request, response) => {
  new ListsController(
    new FetchGenresMoviesUseCase()
  ).handle(request, response);
});

export { router };

