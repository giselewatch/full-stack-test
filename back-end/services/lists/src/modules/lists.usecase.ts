import axios from 'axios';

export class PopularMoviesUseCase {
  async execute() {
    try {
      const response = await axios.get(`https://api.themoviedb.org/3/movie/popular?api_key=${process.env.API_KEY}`);
      return response.data;
    } catch (err) {
      throw new Error(`Failed to fetch popular movies ${err}`);
    }
  }
}

export class FetchGenresMoviesUseCase {
  async execute(genreId?: number) {
    let url = 'https://api.themoviedb.org/3/movie/popular?api_key=${process.env.API_KEY}';
    if (genreId) {
      url = `https://api.themoviedb.org/3/discover/movie?api_key=${process.env.API_KEY}&with_genres=${genreId}`;
    }

    try {
      const response = await axios.get(url);
      return response.data;
    } catch (err) {
      throw new Error(`Failed to fetch data: ${err}`);
    }
  }
}
