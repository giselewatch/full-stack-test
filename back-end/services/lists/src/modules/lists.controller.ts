import { Request, Response } from 'express';
import { FetchGenresMoviesUseCase } from './lists.usecase';

export class ListsController {
  constructor(private fetchGenresMoviesUseCase: FetchGenresMoviesUseCase) {}

  async handle(request: Request, response: Response) {
    const genreId = Number(request.params.id);
    try {
      const data = await this.fetchGenresMoviesUseCase.execute(genreId);
      return response.json(data);
    } catch (err) {
      return response.status(400).json({err});
    }
  }
}