import Cookies from 'js-cookie';
const API_URL = 'http://localhost:3003';

export async function login(email: string, password: string) {
  try {
    const response = await fetch(`${API_URL}/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ email, password }),
    });

    const data = await response.json();

    if (!response.ok) {
      return { success: false, error: data.message };
    }

    Cookies.set('token', data.token);
    Cookies.set('userId', data.id);

    return { success: true, data };
  }
  catch (error) {
    console.error(error);
    return { success: false, error: (error as Error).message };
  }
}

export async function logout() {
  const response = await fetch(`${API_URL}/logout`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    credentials: 'include',
  });

  const data = await response.json();

  if (!response.ok) {
    throw new Error('Network response was not ok');
  }

  return { success: true, data };
}

export async function register(name:string, email:string, phone:string, password:string) {
  try {
    const response = await fetch(`${API_URL}/register`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        name,
        email,
        phone,
        password,
      }),
    });

    const data = await response.json();

    if (!response.ok) {
      return { success: false, error: data.message };
    }

    return { success: true, data };
  } catch (error) {
    console.error(error);
    return { success: false, error: (error as Error).message };
  }
}

export async function getUsers(userId: string) {
  const token = Cookies.get('token');

  const response = await fetch(`${API_URL}/users/${userId}`, {
    headers: {
      'Authorization': `Bearer ${token}`
    },
    credentials: 'include'
  });

  const data = await response.json();
  return data;
}
export async function deleteUser(userId: string) {
  const token = Cookies.get('token');
  const response = await fetch(`${API_URL}/users/${userId}`, {
    method: 'DELETE',
    headers: {
      'Authorization': `Bearer ${token}`,
    },
    credentials: 'include'
  })

  if (!response.ok) {
    throw new Error('Network response was not ok');
  }

  const data = await response.json();
  return data;
}

export async function getPopularMovies() {
  const response = await fetch('http://localhost:3003/lists');
  const data = await response.json();
  return data;
}

export async function getHorrorMovies() {
  const response = await fetch('http://localhost:3003/lists/27');
  const data = await response.json();
  return data;
}
