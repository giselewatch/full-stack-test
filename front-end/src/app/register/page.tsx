"use client";
import { useState } from "react";
import { useRouter } from "next/navigation";
import { register } from "@/services/APIservices";
import styles from "./register.module.scss";
import InputMask from 'react-input-mask';
import Link from "next/link";

export default function Register() {
  const [errorMessage, setErrorMessage] = useState("");
  const router = useRouter();
  const [form, setForm] = useState({
    name: "",
    email: "",
    phone: "",
    password: "",
    confirmPassword: "",
  });

  const { name, email, phone, password, confirmPassword } = form;

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    if (password !== confirmPassword) {
      setErrorMessage("As senhas não coincidem");
      return;
    }

    const response = await register(name, email, phone, password);
    if (response.success) {
      alert('Cadastro realizado com sucesso!');
      router.push("/login");
    } else {
      setErrorMessage("Já existe uma conta cadastrada com esse e-mail");
    }
  };

  return (
    <main className={styles.form__container}>
      <h1>Cadastre-se</h1>

      {errorMessage && <p>{errorMessage}</p>}

      <form className={styles.form} onSubmit={handleSubmit}>
        <input
          type="text"
          name="name"
          placeholder="Nome"
          value={name}
          onChange={handleChange}
          required
        />

        <input
          type="email"
          name="email"
          placeholder="Email"
          value={email}
          onChange={handleChange}
          required
        />

        <InputMask
          type="tel"
          name="phone"
          placeholder="Telefone"
          value={phone}
          onChange={handleChange}
          mask="(99) 99999-9999"
          required
        />

        <input
          type="password"
          name="password"
          placeholder="Senha"
          value={password}
          onChange={handleChange}
          required
        />

        <input
          type="password"
          name="confirmPassword"
          placeholder="Confirme a senha"
          value={confirmPassword}
          onChange={handleChange}
          required
        />

        <button type="submit">Cadastrar</button>

        <p>
          Já tem cadastro? Faça o{" "}
          <Link className="link" href="/login">
            login
          </Link>
        </p>
      </form>
    </main>
  );
}
