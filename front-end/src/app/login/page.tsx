"use client"

import styles from "./login.module.scss";
import { FormEvent, useState } from "react";
import { login } from "@/services/APIservices";
import "./login.module.scss";
import { useRouter } from "next/navigation";
import Link from "next/link";

export default function LoginPage() {
  const router = useRouter();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [errorMessage, setErrorMessage] = useState("");

  const handleSignIn = async (e: FormEvent) => {
    e.preventDefault();

    const response = await login(email, password);
    if (response.success) {
      router.push('/home');
    } else {
      setErrorMessage("Email ou senha inválidos");
    }
  }

  return (
    <main className={styles.login__container}>
      <h1 className={styles.title}>Entrar</h1>

      {errorMessage && <p>{errorMessage}</p>}

      <form className={styles.form} onSubmit={handleSignIn}>
        <input
          type="email"
          name="email"
          placeholder="Email"
          required
          value={email}
          onChange={e => setEmail(e.target.value)}
        />
        <input
          type="password"
          name="password"
          placeholder="Password"
          required
          value={password}
          onChange={e => setPassword(e.target.value)}
        />
        <button type="submit">Login</button>
        <p>
          Não tem cadastro?{" "}
          <Link className="link" href="/register">
            Clique aqui
          </Link>
        </p>
      </form>
    </main>
  );
}