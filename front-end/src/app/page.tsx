'use client'
import Link from "next/link";
import variables from "../styles/variables.module.scss";
import './app.module.scss';
import Image from 'next/image';
import { ThemeToggle } from "@/components/themeToggle/themeToggle";
import styles from "./app.module.scss";

export default function Home() {

  return (
    <>
      <header className={styles.header}>
        <ThemeToggle />
      </header>
      <main className={styles.main} >
        <h1>Moon box</h1>
        <Link className={styles.redirect}  href="/login">Login</Link>
        <Link className={styles.redirect} href="/register">Cadastre-se</Link>
      </main>
    </>
  );
}
