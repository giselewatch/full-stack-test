import CategoryCarousels from "@/components/carousels/carousels";
import NavbarComponent from "@/components/navbar/navbar";

export default function HomePage() {
  return (
    <main>
      <NavbarComponent />
      <CategoryCarousels />
    </main>
  );
}
