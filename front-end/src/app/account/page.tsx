"use client"
import { useState, useEffect } from 'react';
import { User } from '../../types/user';
import { deleteUser, getUsers } from '@/services/APIservices';
import Cookies from 'js-cookie';
import NavbarComponent from '@/components/navbar/navbar';
import style from './account.module.scss';
import { useRouter } from 'next/navigation';

export default function Account() {
  const [user, setUser] = useState<User | null>(null);
  const router = useRouter();

  const handleDeleteAccount = async () => {
    const confirmDelete = window.confirm('Você deseja realmente excluir sua conta?');
    if (!confirmDelete) {
      return;
    }

    const userId = Cookies.get('userId');
    // @ts-ignore
    await deleteUser(userId);
    Cookies.remove('token');
    Cookies.remove('userId');
    router.push('/');
  }

  useEffect(() => {
    const fetchUser = async () => {
      const userId = Cookies.get('userId');
      // @ts-ignore
      const userData = await getUsers(userId);
      setUser(userData);
    };

    fetchUser();
  }, []);

  if (!user) {
    return <div>Loading...</div>;
  }

  return (
    <>
      <NavbarComponent />
      <main className={style.container}>
        <section className={style.info}>
          <h1 className={style.title}>Minha Conta</h1>
          <p><span>Nome:</span> {user.name}</p>
          <p><span>Email:</span> {user.email}</p>
          <p><span>Telefone:</span> {user.phone}</p>
        </section>
        <section className={style.actions}>
          <button className={style.disabled}>Editar informações</button>
          <button onClick={handleDeleteAccount} >Excluir conta</button>
        </section>
      </main>
    </>
  );
}
