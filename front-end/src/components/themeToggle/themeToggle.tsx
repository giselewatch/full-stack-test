import { useEffect, useState } from "react";
import { BsFillMoonStarsFill } from 'react-icons/bs';
import { FaSun } from "react-icons/fa";

export function ThemeToggle() {
  const [darkMode, setDarkMode] = useState(() => {
    const savedTheme = localStorage.getItem('darkMode');
    return savedTheme === 'true' ? true : false;
  });

  useEffect(() => {
    localStorage.setItem('darkMode', darkMode.toString());

    document.documentElement.classList.toggle('light-theme', !darkMode);
  }, [darkMode]);

  const toggleTheme = () => {
    setDarkMode(!darkMode);
  };

  return (
    <button onClick={toggleTheme}>
      {darkMode ? <BsFillMoonStarsFill /> : <FaSun />}
    </button>
  );
}
