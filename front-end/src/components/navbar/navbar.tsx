"use client"
import Link from "next/link";
import styles from "./navbar.module.scss";
import { ThemeToggle } from "../themeToggle/themeToggle";
import { useRouter } from "next/navigation";
import { logout } from "@/services/APIservices";
import Cookies from 'js-cookie';


export default function NavbarComponent() {
  const router = useRouter();

  async function handleLogout() {
    const response = await logout();
    if (response.success) {
      Cookies.remove('token');
      Cookies.remove('userId');
      router.push('/');
    }
  }

  return (
    <header className={styles.header}>
      <nav className={styles.nav}>
        <ThemeToggle />
        <ul>
          <Link className={styles.list} href="/account">Perfil</Link>
          <button onClick={handleLogout}>Logout</button>
        </ul>
      </nav>
    </header>
  );
}
