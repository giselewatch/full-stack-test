'use client'

import { useEffect, useState } from 'react';
import { getPopularMovies, getHorrorMovies } from "@/services/APIservices";
import Slider from 'react-slick';
import Image from 'next/image';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import style from './carousel.module.scss';

interface Movie {
  id: number;
  title: string;
  overview: string;
  poster_path: string;
}

export default function CategoryCarousels() {
  const [popularMovies, setPopularMovies] = useState<Movie[]>([]);
  const [horrorMovies, setHorrorMovies] = useState<Movie[]>([]);

  useEffect(() => {
    const fetchPopularData = async () => {
      const movies = await getPopularMovies();
      setPopularMovies(movies.results);
    };

    const fetchHorrorData = async () => {
      const movies = await getHorrorMovies();
      setHorrorMovies(movies.results);
    }

    fetchPopularData();
    fetchHorrorData();
  }, []);

  const URLImg = 'https://image.tmdb.org/t/p/w500';

  const settings = {
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 5,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
    ]
  };

  return (
    <>
      <section className={style.content}>
        <h1 className={style.title}>Populares</h1>
        <Slider className={style.card}  {...settings}>
          {popularMovies.map(movie => (
            <div className={style.card__content} key={movie.id}>
              <Image
                className={style.card__img}
                src={`${URLImg}${movie.poster_path}`}
                alt="Imagem"
                width={500}
                height={300}
              />
              <p className={style.card__title}>{movie.title}</p>
            </div>
          ))}
        </Slider>
      </section>
      <section className={style.content}>
        <h1 className={style.title}>Filmes de Terror</h1>
        <Slider className={style.card}  {...settings}>
          {horrorMovies.map(movie => (
            <div className={style.card__content} key={movie.id}>
              <Image
                className={style.card__img}
                src={`${URLImg}${movie.poster_path}`}
                alt="Imagem"
                width={500}
                height={300}
              />
              <p className={style.card__title}>{movie.title}</p>
            </div>
          ))}
        </Slider>
      </section>
    </>
  );
}
