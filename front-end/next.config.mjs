import { dirname, join } from 'path';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

/** @type {import('next').NextConfig} */
const nextConfig = {
  webpack5: true,
  experimental: {
    optimizeFonts: false,
    optimizeImages: false,
  },
  sassOptions: {
    includePaths: [join(__dirname, 'styles')],
  },
  images: {
    domains: ["links.papareact.com", "image.tmdb.org"],
  },
};

export default nextConfig;
