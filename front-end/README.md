# Front-end

## Tecnologias Utilizadas

- Next.js
- Sass

## Recursos e funcionalidades adicionais
- Mobile first
- Troca de temas

## Uso
Para iniciar o front-end, execute `npm run dev`.
